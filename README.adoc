= Polyglot Cheat sheet

Cheat sheet for common basic tasks in different programming languages.

Work in progress, this is just a start.

|===
|Task|Python|JavaScript|Java|PHP

|String formatting a|
[source,python]
----
print(f"Results of the {year}")
----
a|
[source,javascript]
----
console.debug(`Results of the ${year}`);
----
a|
a|

|Multiline strings a|
[source,python]
----
question: str = """
many
many lines
"""
----
a|
a|
[source,java]
----
String karl = """many
many lines"""; // Since Java 15
----
a|

|Newlines a|
a|
a|
[source,java]
----
System.lineSeparator();
----
a|
[source,php]
----
PHP_EOL
----

|Nullcheck a|
[source,python]
----
if var is None:
----
a|
a|
[source,java]
----
if (myVar is null) {}
----
a|

|Constants a|
a|
a|
a|

|Logger a|
a|
a|
a|


|Comments a|
[source,python]
----
# Python comment
----
a|
a|
[source,java]
----
/*
Multi-line comment
 */
String s; // Single line comment
----
a|

|===